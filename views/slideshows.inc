<?php

$view = new view();
$view->name = 'slideshows';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'field_collection_item';
$view->human_name = 'Слайдшоу';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'owl-carousel owl-carousel-preset-slideshow setting-no-content-wrapper';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'ещё';
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
$handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'По возрастанию';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'По убыванию';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '100';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['row_class'] = 'slide-item';
$handler->display->display_options['style_options']['default_row_class'] = FALSE;
$handler->display->display_options['style_options']['row_class_special'] = FALSE;
$handler->display->display_options['row_plugin'] = 'fields';
/* Связь: Элемент коллекции полей: Entity with the Слайды (field_slides) */
$handler->display->display_options['relationships']['field_slides_node']['id'] = 'field_slides_node';
$handler->display->display_options['relationships']['field_slides_node']['table'] = 'field_collection_item';
$handler->display->display_options['relationships']['field_slides_node']['field'] = 'field_slides_node';
$handler->display->display_options['relationships']['field_slides_node']['required'] = TRUE;
/* Поле: Элемент коллекции полей: URL */
$handler->display->display_options['fields']['field_slide_url']['id'] = 'field_slide_url';
$handler->display->display_options['fields']['field_slide_url']['table'] = 'field_data_field_slide_url';
$handler->display->display_options['fields']['field_slide_url']['field'] = 'field_slide_url';
$handler->display->display_options['fields']['field_slide_url']['label'] = '';
$handler->display->display_options['fields']['field_slide_url']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_slide_url']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_slide_url']['settings'] = array(
  'field_formatter_class' => '',
);
/* Поле: Элемент коллекции полей: Изображение */
$handler->display->display_options['fields']['field_slide_image']['id'] = 'field_slide_image';
$handler->display->display_options['fields']['field_slide_image']['table'] = 'field_data_field_slide_image';
$handler->display->display_options['fields']['field_slide_image']['field'] = 'field_slide_image';
$handler->display->display_options['fields']['field_slide_image']['label'] = '';
$handler->display->display_options['fields']['field_slide_image']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_slide_image']['element_type'] = '0';
$handler->display->display_options['fields']['field_slide_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_slide_image']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['field_slide_image']['element_default_classes'] = FALSE;
$handler->display->display_options['fields']['field_slide_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_slide_image']['settings'] = array(
  'image_style' => 'large',
  'image_link' => '',
  'field_formatter_class' => '',
);
/* Поле: Глобальный: Пользовательский текст */
$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
$handler->display->display_options['fields']['nothing']['table'] = 'views';
$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
$handler->display->display_options['fields']['nothing']['label'] = '';
$handler->display->display_options['fields']['nothing']['alter']['text'] = '<div class="slide-image">[field_slide_image]</div>';
$handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['nothing']['alter']['path'] = '[field_slide_url]';
$handler->display->display_options['fields']['nothing']['element_type'] = '0';
$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['nothing']['element_wrapper_type'] = '0';
$handler->display->display_options['fields']['nothing']['element_default_classes'] = FALSE;
/* Критерий сортировки: Содержимое: Слайды (field_slides:delta) */
$handler->display->display_options['sorts']['delta']['id'] = 'delta';
$handler->display->display_options['sorts']['delta']['table'] = 'field_data_field_slides';
$handler->display->display_options['sorts']['delta']['field'] = 'delta';
$handler->display->display_options['sorts']['delta']['relationship'] = 'field_slides_node';
/* Критерий фильтра: Содержимое: Nid */
$handler->display->display_options['filters']['nid']['id'] = 'nid';
$handler->display->display_options['filters']['nid']['table'] = 'node';
$handler->display->display_options['filters']['nid']['field'] = 'nid';
$handler->display->display_options['filters']['nid']['relationship'] = 'field_slides_node';
$handler->display->display_options['filters']['nid']['value']['value'] = '2';

/* Display: Основное */
$handler = $view->new_display('block', 'Основное', 'block');

$view->save();
